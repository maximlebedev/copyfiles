package ru.lmd.copyfilenioconsistently;

import java.io.IOException;
import java.nio.file.*;

public class MainNioConsistently {

    private final static String PATH_TO_FIRST_FILE = "src\\ru\\lmd\\files\\FirstFile.txt";
    private final static String PATH_TO_SECOND_FILE = "src\\ru\\lmd\\files\\SecondFile.txt";
    private final static String PATH_TO_READ_FILE = "src\\ru\\lmd\\files\\FileForReadNIO.txt";

    public static void main(String[] args) {
        try {
            long firstTime = System.currentTimeMillis();
            Path sourceOne = Paths.get(PATH_TO_READ_FILE);
            Path targetOne = Paths.get(PATH_TO_FIRST_FILE);
            Path sourceTwo = Paths.get(PATH_TO_READ_FILE);
            Path targetTwo = Paths.get(PATH_TO_SECOND_FILE);
            Files.copy(sourceOne, targetOne, StandardCopyOption.REPLACE_EXISTING);
            Files.copy(sourceTwo, targetTwo, StandardCopyOption.REPLACE_EXISTING);
            long secondTime = System.currentTimeMillis();
            System.out.println("Время выполнения программы: " + (secondTime - firstTime));
        } catch (InvalidPathException e) {
            System.out.println("Ошибка указания пути " + e);
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода " + e);
        }
    }
}
