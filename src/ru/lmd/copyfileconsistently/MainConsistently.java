package ru.lmd.copyfileconsistently;

import ru.lmd.files.WorkFiles;

import java.util.ArrayList;

public class MainConsistently {

    private final static String PATH_TO_FIRST_FILE = "src\\ru\\lmd\\files\\FirstFile.txt";
    private final static String PATH_TO_SECOND_FILE = "src\\ru\\lmd\\files\\SecondFile.txt";

    public static void main(String[] args) {
        long begin = System.currentTimeMillis();

        ArrayList textFromFirstFile = WorkFiles.readFile(PATH_TO_FIRST_FILE);
        WorkFiles.writeFile(PATH_TO_SECOND_FILE, textFromFirstFile);
        ArrayList textFromSecondFile = WorkFiles.readFile(PATH_TO_SECOND_FILE);
        WorkFiles.writeFile(PATH_TO_FIRST_FILE, textFromSecondFile);

        long after = System.currentTimeMillis();

        System.out.print("Consistently copy time: " + (after - begin) + " ms");
    }
}
