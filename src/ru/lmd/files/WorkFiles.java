package ru.lmd.files;

import java.io.*;
import java.util.ArrayList;

public class WorkFiles {

    /**
     * Метод читает данные из файла и записывает их в ArrayList и возвращает
     *
     * @param path путь до файла
     * @return ArrayList с данными из файла
     */
    public static ArrayList readFile(String path) {

        ArrayList text = new ArrayList();

        try (FileReader fileReader = new FileReader(path); BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            String expression;

            while ((expression = bufferedReader.readLine()) != null) {
                text.add(expression);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

    /**
     * Метод записывает входные данные в файл
     *
     * @param path путь до файла
     * @param text входные данные
     */
    public static void writeFile(String path, ArrayList text) {

        try (FileWriter fileWriter = new FileWriter(path); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            for (int i = 0; i < text.size(); i++) {
                bufferedWriter.write(text.get(i) + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
