package ru.lmd.copyfileparallel;

import ru.lmd.files.WorkFiles;

import java.util.ArrayList;

public class ThreadForCopy extends Thread {

    private String pathForRead;
    private String pathForWrite;

    ThreadForCopy(String pathForRead, String pathForWrite) {
        this.pathForRead = pathForRead;
        this.pathForWrite = pathForWrite;
    }

    public void run() {
        long begin = System.currentTimeMillis();

        ArrayList text = WorkFiles.readFile(pathForRead);
        WorkFiles.writeFile(pathForWrite, text);

        long after = System.currentTimeMillis();

        System.out.println(getName() + " copy time: " + (after - begin) + " ms");
    }
}
