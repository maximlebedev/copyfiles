package ru.lmd.copyfileparallel;

public class MainParallel {

    private final static String PATH_TO_FIRST_FILE = "src\\ru\\lmd\\files\\FirstFile.txt";
    private final static String PATH_TO_SECOND_FILE = "src\\ru\\lmd\\files\\SecondFile.txt";

    public static void main(String[] args) {
        long begin = System.currentTimeMillis();

        try {
            Thread firstThread = new ThreadForCopy(PATH_TO_FIRST_FILE, PATH_TO_FIRST_FILE);
            Thread secondThread = new ThreadForCopy(PATH_TO_SECOND_FILE, PATH_TO_SECOND_FILE);

            firstThread.start();
            secondThread.start();
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long after = System.currentTimeMillis();

        System.out.println("Parallel copy time: " + (after - begin) + " ms");
    }
}
