package ru.lmd.copyfilenioparallel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class CopyNIO extends Thread {

    private final static String PATH_TO_READ_FILE = "src\\ru\\lmd\\files\\FileForReadNIO.txt";

    String path;

    CopyNIO(String path){
        this.path = path;
    }

    public void run(){

        long firstTime = System.currentTimeMillis();

        try {
            Path source = Paths.get(PATH_TO_READ_FILE);
            Path target = Paths.get(path);
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long secondTime = System.currentTimeMillis();
        System.out.println(secondTime - firstTime);


    }
}