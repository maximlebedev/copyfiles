package ru.lmd.copyfilenioparallel;

public class MainNioParallel {

    private final static String PATH_TO_FIRST_FILE = "src\\ru\\lmd\\files\\FirstFile.txt";
    private final static String PATH_TO_SECOND_FILE = "src\\ru\\lmd\\files\\SecondFile.txt";

    public static void main(String[] args) {

        long firstTime = System.currentTimeMillis();

        Thread firstThread = new CopyNIO(PATH_TO_FIRST_FILE);
        Thread secondThread = new CopyNIO(PATH_TO_SECOND_FILE);
        firstThread.start();
        secondThread.start();

        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long secondTime = System.currentTimeMillis();
        System.out.println("Время выполнения программы: " + (secondTime - firstTime));

    }
}

